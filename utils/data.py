from utils.singleton import Singleton
from queue import Queue

class Data(metaclass=Singleton):
    def __init__(self):
        self.incidents = Queue()
        self.incidents_ids = []
        self.events = Queue()