from datetime import datetime
from utils.data import Data
import schedule
import time
import threading
import json
import random
from uuid import uuid4
import copy
def generateEvents(event_count):
    data = Data()
    with open('samples\incidents.json', "r") as sample_file:
        samples_json = json.load(sample_file)
    count = 0
    while count < event_count//3:
        # Load a random sample
        temp_sample = random.choice(samples_json)

        temp_sample["type"] = "INCIDENT_CREATION"
        temp_sample["type_id"] = 8075

        ref_incident_uid = random.randint(100000, 1000000)
        event_id = random.randint(100000, 1000000)
        # Replace dynamically generated fields
        created_time = int(datetime.now().timestamp())
        temp_sample["created"] = datetime.utcfromtimestamp(created_time).strftime("%Y-%m-%dT%H:%M:%S.%f+0000")
        temp_sample["time"] = created_time
        incident_id = uuid4()
        temp_sample["incident_uid"] = incident_id
        temp_sample["modified"] = datetime.utcfromtimestamp(created_time).strftime("%Y-%m-%dT%H:%M:%S.%f+0000")
        temp_sample["ref_incident_uid"] = ref_incident_uid
        temp_sample["event_id"] = event_id

        data.incidents.put_nowait(temp_sample)
        data.incidents_ids.append(incident_id)
        count = count + 1

        # Update Incident
        temp_sample_update = copy.deepcopy(temp_sample)
        temp_sample_update["type"] = "INCIDENT_UPDATE"
        temp_sample_update["type_id"] = 8076

        modified_time = created_time + random.randint(1000, 10000)
        temp_sample_update["time"] = modified_time
        incident_id = uuid4()
        temp_sample_update["modified"] = datetime.utcfromtimestamp(modified_time).strftime("%Y-%m-%dT%H:%M:%S.%f+0000")
        
        data.incidents.put_nowait(temp_sample_update)
        count = count + 1

        # Delete Incident
        temp_sample_delete = copy.deepcopy(temp_sample_update)
        temp_sample_delete["type"] = "INCIDENT_CLOSURE"
        temp_sample_delete["type_id"] = 8077

        deleted_time = int(modified_time + random.randint(1000, 10000))
        temp_sample_delete["time"] = deleted_time
        incident_id = uuid4()
        temp_sample_delete["modified"] = datetime.utcfromtimestamp(deleted_time).strftime("%Y-%m-%dT%H:%M:%S.%f+0000")

        data.incidents.put_nowait(temp_sample_delete)
        count = count + 1

    sample_file.close()


class IncidentsEventGen():
    def __init__(self, interval, count):
        self.interval = interval
        self.count = count
        self.canceled = False

    def startGeneration(self):
        self.thread = threading.Thread(target=self.generate, args=(self.count, ))
        self.thread.start()
    
    def generate(self, count):
        self.incidentsGenerator = schedule.every(self.interval).seconds.do(generateEvents, count)
        while(not self.canceled):
            schedule.run_pending()
            time.sleep(1)
    
    def stopGeneration(self):
        self.canceled = True
        schedule.cancel_job(self.incidentsGenerator)