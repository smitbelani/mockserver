from utils.singleton import Singleton

class Checkpoint(metaclass=Singleton):
    def __init__(self):
        self.incident_api_checkpoint = 0
        self.events_api_checkpoint = 0
    def get_incident_api_checkpoint(self):
        return self.incident_api_checkpoint
    def set_incident_api_checkpoint(self, checkpoint):
        self.incident_api_checkpoint = checkpoint
    def get_events_api_checkpoint(self):
        return self.events_api_checkpoint
    def set_events_api_checkpoint(self, checkpoint):
        self.events_api_checkpoint = checkpoint