from datetime import datetime
from utils.data import Data
import schedule
import time
import threading
import json
import random
from uuid import uuid4

def generateEvents(count):
    data = Data()
    with open('samples\events.json', "r") as sample_file:
        samples_json = json.load(sample_file)
    with open('samples\events_associate.json', "r") as associates_sample_file:
        associates_samples = json.load(associates_sample_file)

    for i in range(0, count//2):
        # Generate Incident Event (not 8078)
        temp_event_sample = random.choice(samples_json)
        event_uuid = uuid4()
        event_uid = "{0}:{1}".format(temp_event_sample["type_id"],event_uuid)
        temp_event_sample["id"] = event_uid
        event_time = datetime.now().strftime("%Y-%m-%dT%H:%M:%S.000Z")
        existing_incident_uuid = random.choice(data.incidents_ids)
        temp_event_sample["edr_incident_uids"] = [existing_incident_uuid]
        temp_event_sample["time"] = event_time
        
        # Generate corresponding 8078 event
        temp_associate_event = random.choice(associates_samples)
        temp_associate_event["time"] = int(datetime.now().timestamp())
        temp_associate_event["ref_uid"] = event_uid
        temp_associate_event["incident_uid"] = existing_incident_uuid

        data.events.put_nowait(temp_associate_event)
        data.events.put_nowait(temp_event_sample)

        

    sample_file.close()
    associates_sample_file.close()


class IncidentEventsEventGen():
    def __init__(self, interval, count):
        self.interval = interval
        self.count = count
        self.canceled = False

    def startGeneration(self):
        self.thread = threading.Thread(target=self.generate, args=(self.count, ))
        self.thread.start()
    
    def generate(self, count):
        self.incidentsGenerator = schedule.every(self.interval).seconds.do(generateEvents, count)
        while(not self.canceled):
            schedule.run_pending()
            time.sleep(1)
    
    def stopGeneration(self):
        self.canceled = True
        schedule.cancel_job(self.incidentsGenerator)