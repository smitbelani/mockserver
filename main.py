from typing import Callable, Optional
from fastapi import FastAPI
from fastapi.params import Depends
from routers import incidents, events
from utils import IncidentsEventGen, IncidentEventsEventGen, incident_events_eventgen

app = FastAPI()

INTERVAL = 1 # Second
COUNT = 30 # 30 Incidents per second (Specify in multiple of 3)

incidentsEventGen = IncidentsEventGen(INTERVAL, COUNT)
incidentsEventGen.startGeneration()

EVENTS_INTERVAL = 1 # Second
EVENTS_COUNT = 200 # 2000 Events per second

incidents_events_event_gen = IncidentEventsEventGen(EVENTS_INTERVAL, EVENTS_COUNT)
incidents_events_event_gen.startGeneration()


def create_stop_app_handler(app: FastAPI) -> Callable:
    """To create Fast-API shut-down handler."""
    async def stop_app() -> None:
        """Fast-API shutdown event."""
        incidentsEventGen.stopGeneration()
        incidents_events_event_gen.stopGeneration()
    return stop_app

app.add_event_handler("shutdown", create_stop_app_handler(app))

app.include_router(incidents.router, prefix="/v1")
app.include_router(events.router, prefix="/v1")
