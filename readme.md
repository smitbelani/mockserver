# Steps to start the server

## Requirements
 - Python 3+
 
## Install dependencies
- pip install schedule
- pip install fastapi
- pip install uvicorn[standard]
For fast api installation refer - https://fastapi.tiangolo.com/#installation

## Start the server
- python -m uvicorn main:app --host 0.0.0.0 --port 8000
