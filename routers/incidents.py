import queue
from datetime import datetime
from time import time

import dateutil.parser
from fastapi import APIRouter
from pydantic import BaseModel
from utils import checkpoints, data
from utils.checkpoints import Checkpoint
from utils.data import Data

checkpoints = Checkpoint()

class IncidentsParams(BaseModel):
    start_date: str
    end_date: str
    include_events: bool = False
    limit: int = 2000
    next: int = 0

router = APIRouter()

@router.post("/incidents", tags=["Incidents"])
async def getIncidents(params: IncidentsParams):
    api_called_checkpoint = checkpoints.get_incident_api_checkpoint()
    data = Data()
    print("Incidents Queue Size = " + str(data.incidents.qsize()))
    api_called_checkpoint = checkpoints.set_incident_api_checkpoint(datetime.now())
    list_of_incidents = []
    for i in range(0, params.limit):
        if (data.incidents.qsize() != 0):
            list_of_incidents.append(data.incidents.get_nowait())
        else:
            break
    if data.incidents.qsize() != 0:
        return {"total": data.incidents.qsize(), "next": params.next + params.limit, "incidents": list_of_incidents}
    else:
        return {"total": len(list_of_incidents), "incidents": list_of_incidents}
