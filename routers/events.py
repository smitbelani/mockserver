import queue
from time import time
from pydantic import BaseModel
from fastapi import APIRouter
from datetime import datetime
from utils import checkpoints
from utils import data
from utils.checkpoints import Checkpoint
import dateutil.parser
from utils.data import Data

checkpoints = Checkpoint()

class EventsParams(BaseModel):
    start_date: str
    end_date: str
    include_events: bool = False
    incident_associate: bool = False
    limit: int = 2000
    next: int = 0


router = APIRouter()

@router.post("/incidents/events", tags=["Incidents"])
async def getIncidents(params: EventsParams):
    api_called_checkpoint = checkpoints.get_events_api_checkpoint()
    data = Data()
    print("Events Queue Size = " + str(data.events.qsize()))
    api_called_checkpoint = checkpoints.set_events_api_checkpoint(datetime.now())
    list_of_events = []
    print(params.limit)
    for i in range(0, params.limit):
        if (data.events.qsize() != 0):
            list_of_events.append(data.events.get_nowait())
        else:
            break
    if data.events.qsize() != 0:
        return {"total": data.events.qsize(), "next": params.next + params.limit, "events": list_of_events}
    else:
        return {"total": len(list_of_events), "events": list_of_events}